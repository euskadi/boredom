﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("General Settings")]

    public Animator PlayerAnimator;
    public float m_movementSpeed;
    public float m_backSpeedRatio;
    public float m_rotationSpeed;

    [Header("Movement")]
    [SerializeField]
    protected float m_translation;
    public bool DebugMode_Movement;

    [Header("Jump")]
    public bool DebugMode_Jump;
    public float m_JumpForce;

    private string Animator_Walking_Front = "isWalking_Front";
    private string Animator_Walking_Back = "isWalking_Back";

    private enum MovementType
    {
        front,
        back,
        still
    }


    // Start is called before the first frame update
    void Start()
    {
        //rigidBody = this.GetComponent<Rigidbody>();   
    }

    // Update is called once per frame
    void Update()
    {
        
        m_translation = MovePlayer();

        if (Input.GetButton("Jump"))
        {
            Jump();
        }
        if (DebugMode_Movement)
        {
            print($"Translation = {m_translation}");
        }
        // adjust animation depending on direction of movement
        UpdateAnimation();

    }

    public float MovePlayer()
    {
        // Get the horizontal and vertical axis.

        float m_translation = Input.GetAxis("Vertical") * m_movementSpeed;
        float rotation = Input.GetAxis("Horizontal") * m_rotationSpeed;

        // Make it move per second instead of 10 meters per frame
        m_translation *= Time.deltaTime;
        rotation *= Time.deltaTime;

        if (m_translation < 0) m_translation *= m_backSpeedRatio; // move slower if backwards

        // Move m_translation along the object's z-axis
        transform.Translate(0, 0, m_translation);

        // Rotate around our y-axis
        transform.Rotate(0, rotation, 0);

        return m_translation;
    }

    public void Jump()
    {
        if (DebugMode_Jump)
        {
            print("Jump button was pressed");
        }

        var JumpStrength = m_JumpForce * Time.deltaTime;

        transform.Translate(0, JumpStrength, 0);
        
    }

    public void DetermineMovement()
    {

    }

    private void UpdateAnimation()
    {
        // set walking animations
        PlayerAnimator.SetFloat("transition", m_translation);

        // OVERRIDES


    }

}
