﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactibles : MonoBehaviour
{
    public List<GameObject> m_InteractiblesList;
    public bool m_DebugMode;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        // if other is interactible
        if (other.gameObject.layer == 10)
        {
            print($"Object entered trigger: {other.name}");
            m_InteractiblesList.Add(other.gameObject);
            UpdateOutlinedInteractible();
        }   

    }

    private void OnTriggerExit(Collider other)
    {
        
        // if other is interactible
        if (other.gameObject.layer == 10)
        {
            print($"Object exited trigger: {other.name}");
            m_InteractiblesList.Remove(other.gameObject);

            if (other.gameObject.GetComponent<Outline>())
            {
                other.gameObject.GetComponent<Outline>().enabled = false;
            }
            UpdateOutlinedInteractible();

        }
    }

    private void UpdateOutlinedInteractible()
    {
        foreach(var go in m_InteractiblesList)
        {
            
            // provide visual feedback that component is interactible
            Outline outlineComponent = go.gameObject.GetComponent<Outline>();
            if (outlineComponent)
            {
                outlineComponent.enabled = false; 
            }
        }

        if (m_InteractiblesList.Count > 0)
        {
            var oc = m_InteractiblesList[0].GetComponent<Outline>();
            if (oc)
            {
                oc.enabled = true; 
            }

        }    
    }
}
